import { Message } from 'discord.js'
import { log } from '../logger'

export function deleteAllMessagesInChannel(
    interaction: Message,
    channelId: string
): void {
    if (interaction.channelId === channelId) {
        log(
            '\tDeleted message: ' +
                interaction.member.user.tag +
                ': ' +
                interaction.content
        )
        interaction.delete()
    }
}
