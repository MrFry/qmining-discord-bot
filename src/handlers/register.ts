import fs from 'fs'
import {
    inlineCode,
    ChatInputCommandInteraction,
    Client,
    GuildMember,
} from 'discord.js'

import { assignRole, ephemeralReply } from '../discord-utils'
import {
    HOST,
    ROLE_TO_REGISTER_WITH,
    GUILD_ID,
} from '../../discord-config.json'
import { log } from '../logger'
import { post } from '../utils'

const registrationsFile = './logs/registrations.json'
const checkAndUpdateIfRegistered = (pw: string, tag: string) => {
    if (!fs.existsSync(registrationsFile)) {
        fs.writeFileSync(registrationsFile, JSON.stringify([]))
    }

    let registrations: { pw: string; tag: string }[]
    try {
        registrations = JSON.parse(fs.readFileSync(registrationsFile, 'utf8'))
    } catch (e) {
        log('Error reading registrations file, fixing it', e.message)
        fs.copyFileSync(
            registrationsFile,
            `${registrationsFile}.old.${new Date().getTime()}`
        )
        fs.writeFileSync(registrationsFile, JSON.stringify([]))
        registrations = []
    }

    const alreadyRegistered = registrations.find((x) => {
        return x.pw === pw
    })

    if (!alreadyRegistered) {
        fs.writeFileSync(
            registrationsFile,
            JSON.stringify(
                [
                    ...registrations,
                    {
                        pw: pw,
                        tag: tag,
                    },
                ],
                null,
                2
            )
        )
    }
    return alreadyRegistered
}

const getUserFromGuild = async (
    interaction: ChatInputCommandInteraction,
    client: Client
) => {
    const guild = await client.guilds.fetch(GUILD_ID)
    const dmUser = interaction.user.id
    const member = await guild.members.fetch(dmUser)
    return member
}

const checkIfUsedAndAssignRole = async (
    interaction: ChatInputCommandInteraction,
    client: Client,
    member: GuildMember,
    pw: string
) => {
    const alreadyRegistered = checkAndUpdateIfRegistered(
        pw,
        interaction.user.tag
    )

    if (alreadyRegistered) {
        log(
            '\t' +
                interaction.user.tag +
                ' used pw ' +
                pw +
                ' which is already registered!'
        )
        ephemeralReply(
            interaction,
            'Ezzel a jelszóval már van regisztrált felhasználó! Kérlek lépj kapcsolatba egy @dev-el'
        )
        return
    }

    assignRole(interaction, client, member, ROLE_TO_REGISTER_WITH)
    log('\t' + interaction.user.tag, 'registered!')
}

export const handleRegister = async (
    interaction: ChatInputCommandInteraction,
    client: Client
): Promise<void> => {
    const pw = interaction.options.getString('pw')?.trim()

    if (!pw) {
        ephemeralReply(
            interaction,
            `Jelszó megadása kötelező!
          ${inlineCode('/register <JELSZÓ> ')}`
        )
        return
    }

    let res
    try {
        res = await post<{ success: boolean }>(
            {
                host: HOST,
                port: 443,
                path: `/api/ispwvalid`,
            },
            { pw: pw }
        )
    } catch (e) {
        log('Error with get request:', e.message)
        ephemeralReply(
            interaction,
            'Hiba történt szerverhez kapcsolódás közben! Vagy a szerver nem működik, vagy a megadott paraméterekkel van probléma.'
        )
    }

    try {
        if (res.error) {
            ephemeralReply(
                interaction,
                'Hiba történt jelszó ellenőrzés közben!'
            )
            log('Unexpected server reply:', JSON.stringify(res.data))
            return
        }

        if (!res.data.success) {
            ephemeralReply(interaction, 'Helytelen jelszó!')
            return
        }

        const member =
            interaction.member || (await getUserFromGuild(interaction, client))

        if (!member) {
            ephemeralReply(
                interaction,
                'Nem vagy tagja a szervernek, ahova be szeretnél lépni!'
            )
            return
        }

        checkIfUsedAndAssignRole(interaction, client, member as GuildMember, pw)
    } catch (e) {
        log('Error while processing interaction:', e.message)
        ephemeralReply(
            interaction,
            'Hiba történt az üzenet feldolgozása közben!'
        )
    }
}
