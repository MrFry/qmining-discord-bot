export const log = (...val: string[]): void => {
    console.log(new Date().toLocaleString() + ': ', ...val)
}
