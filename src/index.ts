import fs from 'fs'
import {
    REST,
    Routes,
    Client,
    GatewayIntentBits,
    Partials,
    SlashCommandBuilder,
    ChatInputCommandInteraction,
    GuildMember,
} from 'discord.js'
import {
    TOKEN,
    CLIENT_ID,
    WELCOME_MESSAGE,
    TESTER_ROLE,
} from '../discord-config.json'
import { handleRegister } from './handlers'
import { log } from './logger'
import { assignRole, ephemeralReply, removeRole } from './discord-utils'

try {
    const logsPath = './logs'

    if (!fs.existsSync(logsPath)) {
        fs.mkdirSync(logsPath)
    }
} catch (e) {
    console.log('Some error happened setting up directories', e.message)
}

const commands = [
    {
        name: 'ping',
        description: 'Replies with Pong!',
    },
    {
        name: 'jointesters',
        description: 'Ad "tester" role-t',
    },
    {
        name: 'leavetesters',
        description: 'Eltávolítja rólad a "tester" role-t',
    },
    new SlashCommandBuilder()
        .setName('register')
        .setDescription('Discord szerver regisztráció')
        .addStringOption((option) =>
            option
                .setName('pw')
                .setDescription('Jelszó weboldalhoz')
                .setRequired(true)
        ),
]

const rest = new REST({ version: '10' }).setToken(TOKEN)

async function init() {
    try {
        log('Started refreshing application (/) commands.')

        await rest.put(Routes.applicationCommands(CLIENT_ID), {
            body: commands,
        })

        log('Successfully reloaded application (/) commands.')
    } catch (error) {
        console.error(error)
    }
}

init().then(() => {
    log('Starting bot')
    const client = new Client({
        intents: [
            GatewayIntentBits.Guilds,
            GatewayIntentBits.GuildMembers,
            GatewayIntentBits.GuildMessages,
            GatewayIntentBits.MessageContent,
            GatewayIntentBits.DirectMessages,
        ],
        partials: [Partials.Channel],
    })

    const commandHandlers: {
        [key: string]: (
            interaction: ChatInputCommandInteraction,
            client?: Client
        ) => void
    } = {
        ping: (interaction) => {
            interaction.reply('Pong!')
        },
        register: (interaction) => {
            handleRegister(interaction, client)
        },
        jointesters: (interaction) => {
            if (!interaction?.member?.roles) {
                ephemeralReply(
                    interaction,
                    'Ebben a chatben nem használható ez a parancs!'
                )
                return
            }
            assignRole(
                interaction,
                client,
                interaction.member as GuildMember,
                TESTER_ROLE
            )
        },
        leavetesters: (interaction) => {
            if (!interaction?.member?.roles) {
                ephemeralReply(
                    interaction,
                    'Ebben a chatben nem használható ez a parancs!'
                )
                return
            }
            removeRole(
                interaction,
                client,
                interaction.member as GuildMember,
                TESTER_ROLE
            )
        },
    }

    client.on('ready', () => {
        log(`Logged in as ${client.user.tag}!`)
    })

    // client.on('messageCreate', async (interaction) => {
    //     if (interaction.channel.type === ChannelType.DM) {
    //         console.log(interaction.content)
    //     }
    // })

    client.on('guildMemberAdd', (member) => {
        log('New member:', member.user.tag, 'Sending welcome message ...')
        member.send(WELCOME_MESSAGE)
    })

    client.on('interactionCreate', async (interaction) => {
        if (!interaction.isChatInputCommand()) return
        log(interaction.user.tag, ':', interaction.commandName)

        const handled = Object.entries(commandHandlers).some(
            ([key, handler]) => {
                if (interaction.commandName === key) {
                    handler(interaction, client)
                    return true
                }
                return false
            }
        )
        if (!handled) {
            interaction.reply(`Unhandled command: ${interaction.commandName}`)
        }
    })

    client.login(TOKEN)
})
