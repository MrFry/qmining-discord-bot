import https, { request } from 'https'

interface RequestResult<T> {
    data?: T
    error?: Error
    options?: https.RequestOptions
}

export function get<T>(
    options: https.RequestOptions
): Promise<RequestResult<T>> {
    return new Promise((resolve) => {
        const req = https.get(options, function (res) {
            const bodyChunks: Uint8Array[] = []
            res.on('data', (chunk) => {
                bodyChunks.push(chunk)
            }).on('end', () => {
                const body = Buffer.concat(bodyChunks).toString()
                try {
                    resolve({ data: JSON.parse(body) })
                } catch (e) {
                    console.log(body)
                    resolve({ error: e, options: options })
                }
            })
        })
        req.on('error', function (e) {
            resolve({ error: e, options: options })
            // reject(e)
        })
    })
}

export function post<T = any>(
    options: https.RequestOptions,
    bodyObject: any
): Promise<{
    data?: T
    error?: Error
}> {
    const body = JSON.stringify(bodyObject)

    return new Promise((resolve) => {
        const req = request(
            {
                ...options,
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    'Content-Length': Buffer.byteLength(body),
                },
            },
            (res) => {
                const bodyChunks: string[] = []
                res.setEncoding('utf8')
                res.on('data', (chunk) => {
                    bodyChunks.push(chunk)
                })
                res.on('end', () => {
                    const body = bodyChunks.join()
                    try {
                        resolve({
                            data: JSON.parse(body),
                        })
                    } catch (e) {
                        resolve({ error: e })
                    }
                })
            }
        )

        req.on('error', (e) => {
            resolve({ error: e })
        })

        req.end(body)
    })
}
