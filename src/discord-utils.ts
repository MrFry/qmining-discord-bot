import {
    APIInteractionGuildMember,
    ChatInputCommandInteraction,
    Client,
    GuildMember,
    GuildMemberRoleManager,
} from 'discord.js'
import { GUILD_ID } from '../discord-config.json'

export const ephemeralReply = (
    interaction: ChatInputCommandInteraction,
    content: string
): void => {
    interaction.reply({ content: content, ephemeral: true })
}

export const assignRole = async (
    interaction: ChatInputCommandInteraction,
    client: Client,
    member: GuildMember,
    roleString: string
): Promise<void> => {
    const hasRole = (member.roles as GuildMemberRoleManager).cache.some(
        (role) => role.name.toLowerCase() === roleString
    )
    if (hasRole) {
        ephemeralReply(interaction, `Már van ${roleString} role-od!`)
        return
    }

    const guild = await client.guilds.fetch(GUILD_ID)
    const roleToAdd = guild.roles.cache.find(
        (role) => role.name.toLowerCase() === roleString
    )

    if (roleToAdd) {
        ;(member.roles as GuildMemberRoleManager).add(roleToAdd)
        ephemeralReply(interaction, 'Role megadva!')
    } else {
        console.error(`Role ${roleString} not found!`)
        ephemeralReply(
            interaction,
            'Nem található a megadott role! Kérlek jelezd a bot üzemeltetőnek.'
        )
    }
}

export const removeRole = async (
    interaction: ChatInputCommandInteraction,
    client: Client,
    member: GuildMember | APIInteractionGuildMember,
    roleString: string
): Promise<void> => {
    const hasRole = (member.roles as GuildMemberRoleManager).cache.some(
        (role) => role.name.toLowerCase() === roleString
    )
    if (!hasRole) {
        ephemeralReply(interaction, `Nincs ${roleString} role-od!`)
        return
    }

    const guild = await client.guilds.fetch(GUILD_ID)
    const roleToRemove = guild.roles.cache.find(
        (role) => role.name.toLowerCase() === roleString
    )

    if (roleToRemove) {
        ;(member.roles as GuildMemberRoleManager).remove(roleToRemove)
        ephemeralReply(interaction, 'Role eltávolítva!')
    } else {
        console.error(`Role ${roleString} not found!`)
        ephemeralReply(
            interaction,
            'Nem található a megadott role! Kérlek jelezd a bot üzemeltetőnek.'
        )
    }
}
